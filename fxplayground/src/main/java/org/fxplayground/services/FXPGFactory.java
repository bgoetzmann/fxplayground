/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground.services;

import org.fxplayground.beans.Playground;

/**
 * User: cdea
 * Date: 7/4/2014
 */
public class FXPGFactory {
    private static FXPGCodeManager fxpgCodeManager;
    private static FXPGViewManager fxpgViewManager;
    private static Playground playground;

    private FXPGFactory() {
    }

    public static FXPGCodeManager codeManager() {
        if (fxpgCodeManager == null) {
            fxpgCodeManager = new DefaultFXPGCodeManagerImpl();
        }
        return fxpgCodeManager;
    }
    public static FXPGViewManager viewManager() {
        if (fxpgViewManager == null) {
            fxpgViewManager = new DefaultFXPGViewManagerImpl();
        }
        return fxpgViewManager;
    }

    public static Playground currentPlayground() {
        if (playground == null) {
            playground = new Playground();
        }
        return playground;
    }

    public static Playground newPlayground() {
        playground = new Playground();
        return playground;
    }
    public static void setCurrentPlayground(Playground playground) {
        FXPGFactory.playground = playground;
    }
}

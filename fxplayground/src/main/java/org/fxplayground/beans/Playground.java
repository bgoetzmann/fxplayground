/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground.beans;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.logging.Logger;

/** This represents one FX Playground project.
 * User: cdea
 * Date: 6/30/2014
 */
public class Playground {
    private static final Logger logger = Logger.getLogger(Playground.class.getName());
    public static final String PROJECT_NAME ="name";
    public static final String PROJECT_DESC ="description";
    public static final String PROJECT_USE_FXML ="useFxml";
    public static final String DETACH_OUTPUT ="detachOutput";
    public static final String EXTERNAL_RESOURCES ="resources";
    public static final String MARKUP_LANGUAGE ="markupLanguage";
    public static final String CODE_LANGUAGE ="codeLanguage";

    /* Text from webviews html 5 areas aren't bound to properties */
    public static final String MARKUP_TEXT ="markupText";
    public static final String CODE_TEXT ="codeText";
    public static final String CSS_TEXT ="cssText";
    public static final String MARKUP_TEXT_FILENAME ="markupTextFilename";
    public static final String CODE_TEXT_FILENAME ="codeTextFilename";
    public static final String CSS_TEXT_FILENAME ="cssTextFilename";

    private StringProperty name;
    private StringProperty description;
    private BooleanProperty useFXML;
    private BooleanProperty detachOutput;
    private ObservableList<FXPGResource> resources;
    private StringProperty markupLanguage;
    private ObjectProperty<Language> codeLanguage;
    private StringProperty markupText;
    private StringProperty codeText;
    private StringProperty cssText;
    private StringProperty markupTextFilename;
    private StringProperty codeTextFilename;
    private StringProperty cssTextFilename;

    public Playground() {
        name = new SimpleStringProperty(this, PROJECT_NAME);
        description = new SimpleStringProperty(this, PROJECT_DESC);
        useFXML = new SimpleBooleanProperty(this, PROJECT_USE_FXML, false);
        detachOutput = new SimpleBooleanProperty(this, DETACH_OUTPUT, false);
        resources = FXCollections.observableArrayList();
        markupLanguage = new SimpleStringProperty(this, MARKUP_LANGUAGE);
        codeLanguage = new SimpleObjectProperty<Language>(this, CODE_LANGUAGE);

        markupText = new SimpleStringProperty(this, MARKUP_TEXT);
        codeText = new SimpleStringProperty(this, CODE_TEXT);
        cssText = new SimpleStringProperty(this, CSS_TEXT);

        markupTextFilename = new SimpleStringProperty(this, MARKUP_TEXT_FILENAME);
        codeTextFilename = new SimpleStringProperty(this, CODE_TEXT_FILENAME);
        cssTextFilename = new SimpleStringProperty(this, CSS_TEXT_FILENAME);
    }

    public void reset() {
        name.set("");
        description.set("");
        useFXML.set(false);
        detachOutput.set(false);
        resources.clear();
        //markupLanguage.set("");
        //codeLanguage.set();
        markupText.set("");
        codeText.set("");
        cssText.set("");

    }
    public String getMarkupText() {
        return markupText.get();
    }

    public StringProperty markupTextProperty() {
        return markupText;
    }

    public void setMarkupText(String markupText) {
        this.markupText.set(markupText);
    }

    public String getCodeText() {
        return codeText.get();
    }

    public StringProperty codeTextProperty() {
        return codeText;
    }

    public void setCodeText(String codeText) {
        this.codeText.set(codeText);
    }

    public String getCssText() {
        return cssText.get();
    }

    public StringProperty cssTextProperty() {
        return cssText;
    }

    public void setCssText(String cssText) {
        this.cssText.set(cssText);
    }


    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public boolean getUseFXML() {
        return useFXML.get();
    }

    public BooleanProperty useFXMLProperty() {
        return useFXML;
    }

    public void setUseFXML(boolean useFXML) {
        this.useFXML.set(useFXML);
    }

    public boolean getDetachOutput() {
        return detachOutput.get();
    }

    public BooleanProperty detachOutputProperty() {
        return detachOutput;
    }

    public void setDetachOutput(boolean detachOutput) {
        this.detachOutput.set(detachOutput);
    }

    public ObservableList<FXPGResource> getResources() {
        return resources;
    }

    public String getMarkupLanguage() {
        return markupLanguage.get();
    }

    public StringProperty markupLanguageProperty() {
        return markupLanguage;
    }

    public void setMarkupLanguage(String markupLanguage) {
        this.markupLanguage.set(markupLanguage);
    }

    public Language getCodeLanguage() {
        return codeLanguage.get();
    }

    public ObjectProperty<Language> codeLanguageProperty() {
        return codeLanguage;
    }

    public void setCodeLanguage(Language codeLanguage) {
        this.codeLanguage.set(codeLanguage);
    }

    public String getCssTextFilename() {
        return cssTextFilename.get();
    }

    public StringProperty cssTextFilenameProperty() {
        return cssTextFilename;
    }

    public void setCssTextFilename(String cssTextFilename) {
        this.cssTextFilename.set(cssTextFilename);
    }

    public String getMarkupTextFilename() {
        return markupTextFilename.get();
    }

    public StringProperty markupTextFilenameProperty() {
        return markupTextFilename;
    }

    public void setMarkupTextFilename(String markupTextFilename) {
        this.markupTextFilename.set(markupTextFilename);
    }

    public String getCodeTextFilename() {
        return codeTextFilename.get();
    }

    public StringProperty codeTextFilenameProperty() {
        return codeTextFilename;
    }

    public void setCodeTextFilename(String codeTextFilename) {
        this.codeTextFilename.set(codeTextFilename);
    }
}

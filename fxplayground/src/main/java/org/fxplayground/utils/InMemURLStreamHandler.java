/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Adapted from Jaroslav Tulach at
 * http://www.java2s.com/Open-Source/Java/IDE-Netbeans/welcome/org/netbeans/modules/welcome/MemoryURL.java.htm
 * User: cdea
 * Date: 6/23/2014
 *
 */
public class InMemURLStreamHandler extends URLStreamHandler {
    private static Logger logger = Logger.getLogger(InMemURLStreamHandler.class.getName());
    static {
        class InMemURLStreamHandlerFactory implements URLStreamHandlerFactory {
            public URLStreamHandler createURLStreamHandler(String protocol) {
                if (protocol.startsWith("inmemory")) {
                    return new InMemURLStreamHandler();
                }
                return null;
            }
        }
        InMemURLStreamHandlerFactory f = new InMemURLStreamHandlerFactory();
        URL.setURLStreamHandlerFactory(f);
    }

    private static Map<String,InputStream> contents = new HashMap<String,InputStream>();

    public static void registerURL(String u, String content) {
        contents.put(u, new ByteArrayInputStream(content.getBytes()));
    }

    public static void registerURL(String u, InputStream content) {
        contents.put(u, content);
    }

    public InMemURLStreamHandler() {
    }

    @Override
    protected URLConnection openConnection(URL url) throws IOException {
         return new InMemUrlConnection(url);
    }

    private static final class InMemUrlConnection extends URLConnection {
        private InputStream values;

        public InMemUrlConnection(URL url){
            super(url);
        }
        @Override
        public InputStream getInputStream() throws IOException {
            connect();
            return  values;
        }

        @Override
        public void connect() throws IOException {
            if (values != null) {
                return;
            }
            values = contents.remove(url.toExternalForm());
            if (values == null) {
                throw new IOException("No content: " + url);
            }
            connected = true;
        }
    }
}

